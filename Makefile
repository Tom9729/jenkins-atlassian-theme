DEPLOY_DIR=/var/lib/jenkins/userContent/jenkins-atlassian-theme

.PHONY: all clean deploy

all: grunt
	./grunt

clean:
	rm -rf dist grunt node_modules

grunt:
	npm install
	ln -svf node_modules/grunt-cli/bin/grunt .

deploy:
	sudo mkdir -p $(DEPLOY_DIR)
	sudo cp -vf dist/* $(DEPLOY_DIR)/
