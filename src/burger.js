jQuery(function($){
    // URL to load app list from.
    // TODO: Pull from JIRA REST API?
    var appsUrl = "/userContent/jenkins-atlassian-theme/apps.json";

    // Add DOM element for the burger menu item..
    $("#header").prepend('<div class="burger-button"><span class="burger-button">&#9776;</a></div>');

    var body = $("body");
    
    // Add hidden floating element for the actual menu.
    body.append('<div id="burger-menu"></div>');

    var button = $("div.burger-button");
    var menu = $("#burger-menu");
    
    // Populate menu with items from file.
    $.get(appsUrl).done(function(result) {
        var html = '';
        result.forEach(function(item){
            html += '<a href="' + item.url + '">' + item.name + '</a>';
        });
        menu.append(html);
    });

    // Show menu on click.
    button.on('click', function() {
        button.addClass("selected");
        menu.show();
    });
    
    // Hide menu on any other click.
    body.on('click', function(e) {
        if (!$(e.target).hasClass('burger-button')) {
            button.removeClass("selected");
            menu.hide();
        }
    });
});
