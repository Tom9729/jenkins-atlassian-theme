module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            css: {
                src: [
                    'src/*.css'
                ],
                dest: 'dist/theme.css'
            },
            js : {
                src : [
                    'src/login.js',
                    'src/toggle.js',
                    'src/jobconfig.js',
                    'src/burger.js'
                ],
                dest : 'dist/theme.js'
            }
        },
        cssmin : {
            css:{
                src: 'dist/theme.css',
                dest: 'dist/theme.css'
            }
        },
        uglify : {
            js: {
                files: {
                    'dist/theme.js' : [ 'dist/theme.js' ]
                }
            }
        },
        copy: {
            main: {
                expand: true,
                cwd: 'src',
                dest: 'dist',
                src: [
                    '**/*.png',
                    '**/*.html',
                    '**/*.json'
                ]
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-gh-pages');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.registerTask('default', [ 'copy', 'concat:css', 'cssmin:css', 'concat:js', 'uglify:js' ]);
    grunt.registerTask ('deploy', ['default', 'gh-pages']);
};
